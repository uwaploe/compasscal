# Deep Profiler ACM Compass Calibration Data

Each COMPASSCAL directory contains two files; *acmcal_YYYYmmddTHHMMSS_data.csv* and *acmcal_YYYYmmddTHHMMSS_marks.csv* (*YYYYmmddTHHMMSS* is the timestamp of the start of the data acquisition).

The first file contains all of the ACM magnetometer data while the second contains the data from each reference point (mark). Each record in the first file contains six fields:

  1. Date and time (UTC)
  2. X component of the magnetic flux
  3. Y component of the magnetic flux
  4. Z component of the magnetic flux
  5. X component of the tilt
  6. Y component of the tilt

Each record in the second file contains seven fields:

  1. Date and time (UTC)
  2. X component of the magnetic flux
  3. Y component of the magnetic flux
  4. Z component of the magnetic flux
  5. Reference compass heading for vehicle (not ACM stinger)
  6. X component of the tilt
  7. Y component of the tilt

The `tools` subdirectory contains a Python (version 3) script which will read both files and calculate the 2D hard/soft iron correction for the ACM magnetometer and the heading bias at each of the reference points.

To run the script, you must first install Numpy and SciPy.

``` shellsession
$ acm_compass_proc.py --help
usage: acm_compass_proc.py [-h] [--out OUT] [--simple] YYYYmmddTHHMMSS

Calculate DP ACM compass cal coeff

positional arguments:
  YYYYmmddTHHMMSS  calibration run ID

optional arguments:
  -h, --help       show this help message and exit
  --out OUT        save magnetometer data to the named file
  --simple         use less accurate shortcut to calculate coefficients
```

The output of the script is a JSON format file named *acmcal_YYYYmmddTHHMMSS.json*, an example is shown below.

``` json
{
    "avgbias": -1.549123764038086,
    "bias": [
        -2.0994949340820312,
        -3.3036041259765625,
        -0.7701416015625,
        3.222137451171875,
        2.6728515625,
        -2.863006591796875,
        -5.533973693847656,
        -3.7177581787109375
    ],
    "heading": [
        92.5,
        144.0,
        196.0,
        240.0,
        277.0,
        324.0,
        22.0,
        79.0
    ],
    "xo": 0.030686481753224797,
    "xs": 0.3847401073987872,
    "yo": -0.05204947994504077,
    "ys": 0.37646346370775224
}
```

Fields:

  - **avgbias**: average bias angle (degrees)
  - **bias**: bias angle at each reference heading (degrees)
  - **heading**: reference heading (degrees)
  - **xo**: X offset of ACM magnetometer
  - **xs**: X scale factor of ACM magnetometer
  - **yo**: Y offset of ACM magnetometer
  - **ys**: Y scale factor of ACM magnetometer
