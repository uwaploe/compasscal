#
# Gnuplot batch file to plot the corrected and uncorrected magnetometer
# values. To create the input file, run acm_compass_proc.py with the
# "--out" command-line option:
#
#    acm_compass_proc.py --out mag.out ...
#
# The DP vehicle ID is extracted from the data directory name.
#
reset

id = system("bash -c 'basename $(pwd)' | cut -f3 -d-")
header = sprintf("DP ACM Compass Correction\nDP-%s", id)

set size square
set xrange [-1:1]
set yrange [-1:1]
set grid
set xlabel "HY"
set ylabel "HX"
set title header
plot 'mag.out' u 2:1 w p title 'uncorrected', '' u 4:3 w p title 'corrected'
