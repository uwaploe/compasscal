#!/usr/bin/env python3
#
# Use a optimization method to determine the compass calibration coefficients
# for a DP ACM
#
import numpy as np
from scipy.optimize import minimize
import math
import argparse
import json


# Function to minimize
def errfunc(x, hx, hy):
    mx = (hx - x[0])/x[1]
    my = (hy - x[2])/x[3]
    return math.sqrt(sum(((mx**2 + my**2) - 1)**2))


# Read magnetometer data
def get_mag(fname: str) -> np.array:
    return np.loadtxt(fname,
                      dtype=[('hx', 'f4'), ('hy', 'f4')],
                      usecols=(1, 2),
                      delimiter=',')


def get_marks(fname: str) -> np.array:
    return np.loadtxt(fname,
                      dtype=[('hx', 'f4'), ('hy', 'f4'), ('hdg', 'f4')],
                      usecols=(1, 2, 4),
                      delimiter=',')


def main():
    parser = argparse.ArgumentParser(description='Calculate DP ACM compass cal coeff')
    parser.add_argument("id", metavar="YYYYmmddTHHMMSS",
                        help="calibration run ID")
    parser.add_argument("--out", type=argparse.FileType("w"),
                        help="save magnetometer data to the named file")
    parser.add_argument("--simple", action="store_true",
                        help="use less accurate shortcut to calculate coefficients")
    args = parser.parse_args()

    data = get_mag('acmcal_{}_data.csv'.format(args.id))
    marks = get_marks('acmcal_{}_marks.csv'.format(args.id))

    # Initial coefficient estimates
    ox = np.mean(marks['hx'])
    oy = np.mean(marks['hy'])
    sx = np.amax(np.fabs(marks['hx'] - ox))
    sy = np.amax(np.fabs(marks['hy'] - oy))

    print("Data points: {:d}".format(len(data)))
    x0 = np.array([ox, sx, oy, sy])
    if not args.simple:
        res = minimize(errfunc, x0, args=(data['hx'], data['hy']), method='nelder-mead',
                       options={'xatol': 1e-8, 'disp': True})
        coeff = res.x
        if not res.success:
            print("Cannot calculate coefficients")
            return
    else:
        coeff = x0
    print("Coefficients: ", coeff)

    # Calculate the bias angles
    mx = (marks['hx'] - coeff[0])/coeff[1]
    my = (marks['hy'] - coeff[2])/coeff[3]
    # ACM heading is 25 degrees counter-clockwise from the vehicle heading
    hdg_mark = marks['hdg'] - 25
    hdg_cartesian = np.rad2deg(np.arctan2(mx, my))
    hdg_nautical = 90. - hdg_cartesian
    bias = np.fmod(hdg_nautical - hdg_mark, 360.)
    # Map bias values to -180, 180
    high = bias > 180
    low = bias < -180
    bias[high] -= 360
    bias[low] += 360

    with open('acmcal_{}.json'.format(args.id), 'w') as f:
        result = dict(xo=float(coeff[0]), xs=float(coeff[1]),
                      yo=float(coeff[2]), ys=float(coeff[3]),
                      heading=np.mod(hdg_mark, 360).tolist(),
                      bias=bias.tolist(), avgbias=float(np.mean(bias)))
        json.dump(result, f, sort_keys=True, indent=4)

    if args.out is not None:
        mx = (data['hx'] - coeff[0])/coeff[1]
        my = (data['hy'] - coeff[2])/coeff[3]
        np.savetxt(args.out, np.column_stack((data['hx'], data['hy'], mx, my)),
                   fmt='%.2f', delimiter=' ', newline='\n')


if __name__ == '__main__':
    main()
